/**
 *
 *      +----------------------------------------------------------------------------------------------------------------+
 *      |                                             Copyright - Northest Missouri State University                  | 
*       |-----------------------------------------------------------------------------------------------------------------|
 *      |Team #5                                                                                                                          | 
 *      |Unit #5  : Santhosh Dulam,Sai Varun,Keerthi                                                                                        | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Description: This page manages the routing - connecting the requests the the controllers that handle them.                                                                     | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                    |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |   Santhosh Dulam                          0.0.1(Initial)                 initial                | 
 *      |                                                                                                                                       | 
 *      |                                                                                                                                       | 
 *      +-----------------------------------------------------------------------------------------------------------------+
 */



 /**
* @index.js - manages all routing
*
* router.get when assigning to a single request
* router.use when deferring to a controller
*
* @requires express
*/

const express = require('express')
const LOG = require('../utils/logger.js')  // comment out until exists
LOG.debug('START routing')   // comment out until exists
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
 LOG.debug('Request to /')  // comment out until exists
 res.render('index.ejs', { title: 'CGE04' }) //  use your sec num
})

// Defer path requests to a particular controller
router.use('/about', require('../controllers/about.js'))
router.use('/estimate', require('../controllers/estimate.js'))
router.get('/contact',  (req, res) =>{
    res.render('contact/index', {
        title: 'Contact Us'
      })    
})
//router.use('/user', require('../controllers/user.js'))

LOG.debug('END routing')
module.exports = router