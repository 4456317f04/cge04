/*
*Unit 8
*Chester Condray, Elicia Reuscher
*/

process.env.NODE_ENV = 'test'
const app = require('../../app.js')
var User = require('../../models/user.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
var request = require('supertest')
const EMAIL = 'test@test.com'
const PASSWORD = 'Password_1'

LOG.debug('Starting test/controllers/user-spec.js.')

//Login----mocha.describe('API Tests
    //In describe----mocha.describe('GET /login'----Unit 01

   
    mocha.describe('API Tests - User Controller', function () {
        mocha.beforeEach(function (done) {
          var user = new User({
            username: EMAIL,
            password: PASSWORD
          })
      
          user.save(function (error) {
            if (error) {
              LOG.error('Error: ' + error.message)
            } else { LOG.info('no error') }
            done()
          })
        }) 

    //FindOne----mocha.it('find a user by email'----Unit 02
    mocha.it('find a user by email', function (done) {
      User.findOne({ email: EMAIL }, function (err, user) {
        if (err) { return done(err, 'Error finding user.') }
        if (user == null) { return done(err, 'No user found.') }
        expect(user.email).to.equal(EMAIL)
        LOG.info('   email: ', user.email)
        done()
      })
    })

    //After----mocha.after(----Unit 02

    mocha.after(function () {
    return User.remove().exec()
  })

  mocha.describe('GET /signup', function () {
    var token

        //Signup----mocha.before(----Unit 03
        mocha.before(function (done) {
          request(app)
            .post('/signup')
            .send({
              email: EMAIL,
              password: PASSWORD
            })
            .expect(302) // found
            .expect('Content-Type', 'text/plain; charset=utf-8')
            .end(function (err, res) {
              if (err) return done(err)
              token = res.body.token
              done()
            })
        })
        

        //Account----mocha.it('should respond...'----Unit 04

        //Logout----mocha.it('should respond...'----Unit 04
    })
})