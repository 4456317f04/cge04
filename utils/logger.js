/**

 *

 *      +----------------------------------------------------------------------------------------------------------------+

 *      |                                             Copyright - Northest Missouri State University                  | 

*       |-----------------------------------------------------------------------------------------------------------------|

 *      |Team 06                                                                                                                           | 

 *      |Unit 07  : Rohith Kumar Agiru , Kishan Kalburgi Srinivas                                                                                         | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |Description: This program records all the logs that occur during execution                                                               | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |    NAME                            VERSION                       CHANGES                                    |

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |    Kishan Kalburgi Srinivas      0.0.1(Initial)                      change log                        | 

 *      |                                                                                                                                       | 

 *      |                                                                                                                                       | 

 *      +-----------------------------------------------------------------------------------------------------------------+

 */
const winston = require('winston')
const fs = require('fs')
const path = require('path')

const logDir = 'logs'

if (!fs.existsSync(logDir)) { fs.mkdirSync(logDir)}

const logger = new (winston.Logger)({
 level: 'debug',
 transports: [
   new (winston.transports.Console)({ json: false, timestamp: true }),
   new winston.transports.File({ filename: path.join(logDir, '/debug.log'), json: false })
 ],
 exceptionHandlers: [
   new (winston.transports.Console)({ json: false, timestamp: true, humanReadableUnhandledException: true }),
   new winston.transports.File({ filename: path.join(logDir, '/debug.log'), json: false, humanReadableUnhandledException: true })
 ],
 exitOnError: false
})

module.exports = logger